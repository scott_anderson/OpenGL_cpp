// builds and returns a translation matrix
mat4 build_translate(float x, float y, float z)
{
	mat4 trans = mat4 (
	1.0, 0.0, 0.0, 0.0,
	0.0, 1.0, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	x,   y,   z,   1.0
	);
	return trans;
}


// builds and returns a x rotation matrix
mat4 build_rotate_x(float rad)
{
	mat4 xrot = mat4 (
	1.0, 0.0, 0.0, 0.0,
	0.0, cos(rad), -sin(rad), 0.0,
	0.0, sin(rad), cos(rad), 0.0,
	0.0, 0.0, 0.0, 1.0
	);
	return xrot;
}


// builds and returns y rotation matrix
mat4 build_rotate_y(float rad)
{
	mat4 yrot = mat4 (
	cos(rad), 0.0, sin(rad), 0.0,
	0.0, 1.0, 0.0, 0.0,
	-sin(rad), 0.0, cos(rad), 0.0,
	0.0, 0.0, 0.0, 1.0
	);
	return yrot;
}

// builds and returns z rotation matrix
mat4 build_rotate_z(float rad)
{
	mat4 zrot = mat4 (
	cos(rad), -sin(rad), 0.0, 0.0,
	sin(rad), cos(rad), 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.0, 0.0, 0.0, 1.0
	);
	return zrot;
}

// builds and returns a scale matrix
mat4 build_scale(float x, float y, float )
{
	mat4 scale = mat4 (
	x,   0.0, 0.0, 0.0,
	0.0,   y, 0.0, 0.0,
	0.0, 0.0,   z, 0.0,
	0.0, 0.0, 0.0, 1.0
	);
	return scale;
}


