/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   utils.cpp
 * Author: cscott
 * 
 * Created on October 24, 2019, 11:01 PM
 */
#include "utils.h"

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <glm/glm.hpp>

//#include "stb_image.h"

#include <sys/types.h>
#include <sys/stat.h>
#ifdef __LINUX__
#include <unistd.h>
#endif

#include <soil2/SOIL2.h>


glm::mat4 look_at(glm::vec3 eye, glm::vec3 target, glm::vec3 up)
{
	glm::vec3 zaxis = glm::normalize(eye - target);    // The "forward" vector.
	glm::vec3 xaxis = glm::normalize(glm::cross(up, zaxis));// The "right" vector.
	glm::vec3 yaxis = glm::cross(zaxis, xaxis);     // The "up" vector.

	// Create a 4x4 orientation matrix from the right, up, and forward vectors
	// This is transposed which is equivalent to performing an inverse 
	// if the matrix is orthonormalized (in this case, it is).
	glm::mat4 orientation = {
	   glm::vec4(xaxis.x, yaxis.x, zaxis.x, 0),
	   glm::vec4(xaxis.y, yaxis.y, zaxis.y, 0),
	   glm::vec4(xaxis.z, yaxis.z, zaxis.z, 0),
	   glm::vec4(0,       0,       0,     1)
	};

	// Create a 4x4 translation matrix.
	// The eye position is negated which is equivalent
	// to the inverse of the translation matrix. 
	// T(v)^-1 == T(-v)
	glm::mat4 translation = {
		glm::vec4(1,      0,      0,   0),
		glm::vec4(0,      1,      0,   0),
		glm::vec4(0,      0,      1,   0),
		glm::vec4(-eye.x, -eye.y, -eye.z, 1)
	};

	// Combine the orientation and translation to compute 
	// the final view matrix. Note that the order of 
	// multiplication is reversed because the matrices
	// are already inverted.
	return (orientation * translation);
}


//=====================================================
// Create a shader program to be loaded onto the GPU
//=====================================================
GLuint create_shader_program(const std::string& vertex_shader, const std::string& fragment_shader)
{
	std::string vshader_source_str = load_shader(vertex_shader.c_str());
	std::string fshader_source_str = load_shader(fragment_shader.c_str());

	const char* vshader_source_src = vshader_source_str.c_str();
	const char* fshader_source_src = fshader_source_str.c_str();

	GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);

	// apply the shader sources to each shader we have
	glShaderSource(vshader, 1, &vshader_source_src, NULL);
	glShaderSource(fshader, 1, &fshader_source_src, NULL);

	// compile the vertex shader
	glCompileShader(vshader);
	if (shader_compiler_problems(vshader, "vertex shader")) { exit(EXIT_FAILURE); }
	// compile the fragment shader
	glCompileShader(fshader);
	if (shader_compiler_problems(fshader, "fragment shader")) { exit(EXIT_FAILURE); }

	// create our program
	GLuint vf_program = glCreateProgram();
	// attach the two shaders
	glAttachShader(vf_program, vshader);
	glAttachShader(vf_program, fshader);
	// link them into a program
	glLinkProgram(vf_program);
	if (program_linker_problems(vf_program, "vf_program"))
	{
		exit(EXIT_FAILURE);
	}
	// always detach shaders after a successful linke
	glDetachShader(vf_program, vshader);
	glDetachShader(vf_program, fshader);
	return vf_program;
}

//=====================================================
// check for an openGL error.
//=====================================================
bool check_for_opengl_error()
{
	bool found_error = false;
	int gl_err = glGetError();
	while (gl_err != GL_NO_ERROR)
	{
		std::cout << "glError: " << gl_err << std::endl;
		found_error = true;
		gl_err = glGetError();
	}
	return found_error;
}

//=====================================================
// check for actual problems when compiling each shader
//=====================================================
bool shader_compiler_problems(GLuint shader, const std::string& shader_name)
{
	GLint isCompiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> error_log(maxLength);
		glGetShaderInfoLog(shader, maxLength, &maxLength, &error_log[0]);
		std::string s(begin(error_log), end(error_log));
		std::cout << shader_name << ": " << s << std::endl;

		// Provide the infolog in whatever manor you deem best.
		// Exit with failure.
		glDeleteShader(shader); // Don't leak the shader.
		return true;
	}
	return false;
}

//=====================================================
// check for linker problems when creating a shader program
//=====================================================
bool program_linker_problems(GLuint program, const std::string& program_name)
{
	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> info_log(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &info_log[0]);
		std::string s(begin(info_log), end(info_log));
		std::cout << program_name << ": " << s << std::endl;

		// The program is useless now. So delete it.
		glDeleteProgram(program);

		// Provide the infolog in whatever manner you deem best.
		// Exit with failure.
		return true;
	}
	return false;
}

//=====================================================
// does the file exist
//=====================================================
bool file_exists(const std::string& filename)
{
	struct stat buf;
	if (stat(filename.c_str(), &buf) != -1)
	{
		return true;
	}
	return false;
}

//=====================================================
// load a shader file from disk
//=====================================================
std::string load_shader(const char* file_path)
{
	std::string content;
	std::ifstream file_stream(file_path, std::ios::in);
	std::string line;
	if (file_exists(file_path))
	{
		while (!file_stream.eof())
		{
			std::getline(file_stream, line);
			content.append(line + "\n");
		}
		file_stream.close();
	}
	return content;
}


#if 1

GLuint load_cube_map(const char* map_dir)
{
	GLuint texture_ref;
	std::string xp = map_dir; xp = xp + "/xp.jpg";
	std::string xn = map_dir; xn = xn + "/xn.jpg";
	std::string yp = map_dir; yp = yp + "/yp.jpg";
	std::string yn = map_dir; yn = yn + "/yn.jpg";
	std::string zp = map_dir; zp = zp + "/zp.jpg";
	std::string zn = map_dir; zn = zn + "/zn.jpg";

	texture_ref = SOIL_load_OGL_cubemap(xp.c_str(), xn.c_str(), yp.c_str(), yn.c_str(), zp.c_str(), zn.c_str(),
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);

	if (texture_ref == 0) 
		std::cout << "didnt find cube map image file" << std::endl;

	glBindTexture(GL_TEXTURE_CUBE_MAP, texture_ref);
	// reduce seams
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return texture_ref;
}

GLuint load_texture(const char* tex_image_path)
{
	GLuint texture_ref;
	texture_ref = SOIL_load_OGL_texture(tex_image_path, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	if (texture_ref == 0) 
		std::cout << "didnt find texture file " << tex_image_path << std::endl;

	// ----- mipmap/anisotropic section
	glBindTexture(GL_TEXTURE_2D, texture_ref);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	if (glewIsSupported("GL_EXT_texture_filter_anisotropic")) 
	{
		GLfloat anisoset = 0.0f;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anisoset);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisoset);
	}
	// ----- end of mipmap/anisotropic section
	return texture_ref;
}

// GOLD material - ambient, diffuse, specular, and shininess
float* gold_ambient() { static float a[4] = { 0.2473f, 0.1995f, 0.0745f, 1 }; return (float*)a; }
float* gold_diffuse() { static float a[4] = { 0.7516f, 0.6065f, 0.2265f, 1 }; return (float*)a; }
float* gold_specular() { static float a[4] = { 0.6283f, 0.5559f, 0.3661f, 1 }; return (float*)a; }
float gold_shininess() { return 51.2f; }

// SILVER material - ambient, diffuse, specular, and shininess
float* silver_ambient() { static float a[4] = { 0.1923f, 0.1923f, 0.1923f, 1 }; return (float*)a; }
float* silver_diffuse() { static float a[4] = { 0.5075f, 0.5075f, 0.5075f, 1 }; return (float*)a; }
float* silver_specular() { static float a[4] = { 0.5083f, 0.5083f, 0.5083f, 1 }; return (float*)a; }
float silver_shininess() { return 51.2f; }

// BRONZE material - ambient, diffuse, specular, and shininess
float* bronze_ambient() { static float a[4] = { 0.2125f, 0.1275f, 0.0540f, 1 }; return (float*)a; }
float* bronze_diffuse() { static float a[4] = { 0.7140f, 0.4284f, 0.1814f, 1 }; return (float*)a; }
float* bronze_specular() { static float a[4] = { 0.3936f, 0.2719f, 0.1667f, 1 }; return (float*)a; }
float bronze_shininess() { return 25.6f; }
#endif