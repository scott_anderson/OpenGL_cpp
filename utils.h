/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   utils.h
 * Author: cscott
 *
 * Created on October 24, 2019, 11:01 PM
 */
#ifndef OPENGL_UTILS
#define OPENGL_UTILS

#define GLEW_STATIC
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>

//===========================================
// Module wide color definitions
//===========================================
#define RED		1.0, 0.0, 0.0, 1.0
#define GREEN	0.0, 1.0, 0.0, 1.0
#define BLUE	0.0, 0.0, 1.0, 1.0
#define BLACK	0.0, 0.0, 0.0, 1.0


//===========================================
// function declarations
//===========================================
GLuint create_shader_program(const std::string& vertex_shader, const std::string& fragment_shader); 

bool check_for_opengl_error();

bool shader_compiler_problems(GLuint shader, const std::string& shader_name);

bool program_linker_problems(GLuint program, const std::string& program_name);

bool file_exists(const std::string& filename);

std::string load_shader(const char* file_path);

glm::mat4 look_at(glm::vec3 eye, glm::vec3 target, glm::vec3 up);

#endif
