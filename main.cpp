/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: cscott
 *
 * Created on October 24, 2019, 10:52 PM
 */


#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>

#include <chrono>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <utils.h>

#include <sys/types.h>
#include <sys/stat.h>
#ifdef __LINUX__
#include <unistd.h>
#endif


//==============================================================
// local defines
//==============================================================
#define NUM_VAOS	1
#define NUM_VBOS	2

//==============================================================
// local variables
//==============================================================
glm::vec3 camera_pos;
glm::vec3 target; // u = camera - target
glm::vec3 up; // n_vec


GLuint		rendering_program;
GLuint		vao[NUM_VAOS];
GLuint		vbo[NUM_VBOS];

GLuint		mv_loc;
GLuint		proj_loc;
int			width;
int			height;
float		aspect;
glm::mat4	p_mat;
glm::mat4	t_mat;
glm::mat4	r_mat;
glm::mat4	v_mat;
glm::mat4	m_mat;
glm::mat4	mv_mat;

//==============================================================
// functions
//==============================================================

void setup_verticies(void)
{
	//-----------------------------------------------
	// local model space coordinates
	//-----------------------------------------------
	float vertex_positions[108] =
	{ -1.0f,  1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f, 1.0f,  1.0f, -1.0f, -1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f, 1.0f, -1.0f,  1.0f, 1.0f,  1.0f, -1.0f,
		1.0f, -1.0f,  1.0f, 1.0f,  1.0f,  1.0f, 1.0f,  1.0f, -1.0f,
		1.0f, -1.0f,  1.0f, -1.0f, -1.0f,  1.0f, 1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f, -1.0f,  1.0f,  1.0f, 1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f, -1.0f, -1.0f, -1.0f, -1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f, -1.0f,  1.0f, -1.0f, -1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,  1.0f, -1.0f,  1.0f,  1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f, 1.0f,  1.0f, -1.0f, 1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f, -1.0f,  1.0f,  1.0f, -1.0f,  1.0f, -1.0f
	};
	glGenVertexArrays(1, vao);
	glBindVertexArray(vao[0]);
	glGenBuffers(NUM_VBOS, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_positions), vertex_positions, GL_STATIC_DRAW);

}

//==============================================================
// initialize varables and openGL state
//==============================================================
static void init(GLFWwindow* window)
{
	//-----------------------------------------------
	// startig camera location
	//-----------------------------------------------
	float		camera_X;
	float		camera_Y;
	float		camera_Z;

	//-----------------------------------------------
	// starting cube location
	//-----------------------------------------------
	float		cube_loc_X;
	float		cube_loc_Y;
	float		cube_loc_Z;

	//-----------------------------------------------
	// create the shader program
	//-----------------------------------------------
	rendering_program = create_shader_program("vertex_shader.glsl", "fragment_shader.glsl");

	//-----------------------------------------------
	// initialize the starting locations
	//-----------------------------------------------
	camera_X = 0.0f;
	camera_Y = 0.0f;
	camera_Z = 8.0f;

	cube_loc_X = 0.0f;
	cube_loc_Y = 0.0f;// -2.0f;
	cube_loc_Z = 0.0f;

	//-----------------------------------------------
	// build the perspective matrix
	//-----------------------------------------------
	glfwGetFramebufferSize(window, &width, &height);
	aspect = (float)width / (float)height;
	p_mat = glm::perspective(1.0472f, aspect, 0.1f, 1000.0f);

	camera_pos	= { camera_X,	camera_Y,	camera_Z,	};
	target		= { cube_loc_X, cube_loc_Y, cube_loc_Z	}; // u = camera - target
	up			= { 0.0f,		1.0f,		0.0f		}; // n_vec

	//-----------------------------------------------
	// create the cube from a local coordinate space
	//-----------------------------------------------
	setup_verticies();
}

//==============================================================
// render/display openGL commands
//==============================================================
static void display(GLFWwindow* window, double current_time)
{
	glClear(GL_DEPTH_BUFFER_BIT);
	//glClearColor(BLACK);
	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(rendering_program);

	//-----------------------------------------------
	// get the uniform variables locations from the shaders
	//-----------------------------------------------
	mv_loc = glGetUniformLocation(rendering_program, "mv_matrix");
	proj_loc = glGetUniformLocation(rendering_program, "proj_matrix");

	//-----------------------------------------------
	// set camera position, and 
	// build the view transform matrix
	//-----------------------------------------------
	v_mat = look_at(camera_pos, target, up);

	//-----------------------------------------------
	// translate and rotate the model
	//-----------------------------------------------
	//t_mat = glm::mat4(1.0f);
	//r_mat = glm::mat4(1.0f);
	t_mat = glm::translate(glm::mat4(1.0f), glm::vec3(sin(0.35f * current_time) * 2.0f, cos(0.52f * current_time) * 2.0f, sin(0.7f * current_time) * 2.0f));
	r_mat = glm::rotate(glm::mat4(1.0f), 1.75f * (float)current_time, glm::vec3(0.0f, 1.0f, 0.0f));
	r_mat = glm::rotate(r_mat, 1.75f * (float)current_time, glm::vec3(1.0f, 0.0f, 0.0f));
	r_mat = glm::rotate(r_mat, 1.75f * (float)current_time, glm::vec3(0.0f, 0.0f, 1.0f));
	//-----------------------------------------------
	// build the model matrix
	//-----------------------------------------------
	m_mat = t_mat * r_mat;

	//-----------------------------------------------
	// build model view matrix
	//-----------------------------------------------
	mv_mat = v_mat * m_mat;

	//-----------------------------------------------
	// copy perspective and MV matrices to corresponding uniform variables
	//-----------------------------------------------
	glUniformMatrix4fv(mv_loc, 1, GL_FALSE, glm::value_ptr(mv_mat));
	glUniformMatrix4fv(proj_loc, 1, GL_FALSE, glm::value_ptr(p_mat));

	//-----------------------------------------------
	// associate VBO with the corresponding vertex attribute in the vertex shader.
	//-----------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	//-----------------------------------------------
	// adjust OpenGL settings and draw model
	//-----------------------------------------------
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDrawArrays(GL_TRIANGLES, 0, 36);
}

void window_size_callback(GLFWwindow* win, int newWidth, int newHeight)
{
	aspect = (float)newWidth / (float)newHeight;
	glViewport(0, 0, newWidth, newHeight);
	p_mat = glm::perspective(1.0472f, aspect, 0.1f, 1000.0f);
}

//==============================================================
//
// The main entry point of our program
//
//==============================================================
int main(void)
{
	/* Initialize the library */
	if (!glfwInit())
		return -1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	/* Create a windowed mode window and its OpenGL context */
	GLFWwindow* window = glfwCreateWindow(600, 600, "OpenGL", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}
	/* Make the window's context current */
	glfwMakeContextCurrent(window);
	if (glewInit() != GLEW_OK)
	{
		return -1;
	}
	glfwSwapInterval(1);
	glfwSetWindowSizeCallback(window, window_size_callback);
	init(window);

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		display(window, glfwGetTime());
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}
